# heycenter

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### Simulación de inicio de sesión
```
introduzca cualquier cadena de texto con formato de correo electrónico en el campo "Correo electrónico"
introduzca cualquier cadena de texto en el campo "Contraseña"
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

