import Vue from "vue";
import VueRouter from "vue-router";

import LoginView from "@/views/LoginView.vue";

import PanelLayout from "@/views/panel/PanelLayout.vue";
import ChatsView from "@/views/panel/ChatsView.vue";
import GroupsView from "@/views/panel/GroupsView.vue";
import HistoryView from "@/views/panel/HistoryView.vue";
import MyProfileView from "@/views/panel/MyProfileView.vue";

import NotFoundView from "@/views/NotFoundView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: '/login',
    name: "index",
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/panel",
    component: PanelLayout,
    children: [
      { name: 'chats' , path: 'chats', component: ChatsView },
      { name: 'grupos' , path: 'grupos', component: GroupsView },
      { name: 'historial' , path: 'historial', component: HistoryView },
      { name: 'mi-perfil' , path: 'mi-perfil', component: MyProfileView }
    ]
  },
  {  path: '/:pathMatch(.*)', component: NotFoundView }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
